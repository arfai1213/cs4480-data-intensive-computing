import org.apache.spark.graphx._
import org.apache.spark.graphx.util.GraphGenerators

val graph: Graph[Int, Int] = GraphLoader.edgeListFile(sc, "ex_data/roadnet/roadNet-CA.txt")
val sourceId: VertexId = 94 // The ultimate sour
var destId: VertexId = 124

// Initialize the graph such that all vertices except the root have distance infinity.
val initialGraph : Graph[(Int, List[VertexId]), Int] = graph.mapVertices(
  (id, _) => 
    if (id == sourceId) 
      (0, List[VertexId](sourceId)) 
    else 
      (Int.MaxValue, List[VertexId]())
)

val sssp = initialGraph.pregel((Int.MaxValue, List[VertexId]()), Int.MaxValue, EdgeDirection.Out)(
  // Vertex Program
  (id, dist, newDist) => if (dist._1 < newDist._1) dist else newDist, 

  // Send Message
  triplet => {
    if (triplet.srcAttr._1 < triplet.dstAttr._1 - triplet.attr ) {
      Iterator((triplet.dstId, (triplet.srcAttr._1 + triplet.attr , triplet.srcAttr._2 :+ triplet.dstId)))
    } else {
      Iterator.empty
    }
  },
  //Merge Message
    (a, b) => if (a._1 < b._1) 
      a else b
  )
      
val result = sssp.vertices.filter{
  case (v, (w, list)) =>  w != Int.MaxValue && v == destId
}

if(result.collect.size != 0){
  val list = result.collect.map(x=> x._2)
  val path = list.map(x=> x._2)

  println("The path from Node " + sourceId + " to Node " + destId + " :")
  path.foreach(println)
}else{
  println("Node " + sourceId + " have no path to Node " + destId)
}